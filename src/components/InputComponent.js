import React, { Component } from 'react'

export class InputComponent extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             value: '',
        }
    }
    handleChange = (event) => {
        this.setState({value: event.target.value});
        this.props.handleValdiation(event.target.value);
    }
    render() {
        return (
            <div style={inputStyle}>
                 <label>{this.props.label}</label>
                <input type={this.props.type} value={this.state.value} onChange={this.handleChange}></input>
            </div>
        )
    }
}
const inputStyle = {
    display: "flex",
    flexDirection:"column",
    gap: "5px",
}
export default InputComponent
