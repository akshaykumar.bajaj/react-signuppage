import React, { Component } from 'react'
import validator from 'validator';
import InputComponent from './InputComponent';
import './signUp.css';
// InputComponent

export class SignUpForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            input: {
                firstname: "",
                lastname: "",
                email: "",
                password: "",
            },
            error: {
                firstname: true,
                lastname: true,
                email: true,
                password: true,
                confirmPass: true,
            }
        }
    }
    firstNameValidator = (name) => {
        if (name.length < 3) {
            this.setState({
                error: {
                    ...this.state.error,
                    firstname: true,
                }
            })
        }
        else {
            this.setState({
                input: {
                    ...this.state.name,
                    firstname: name,
                },
                error: {
                    ...this.state.error,
                    firstname: false,
                }
            })
        }
    }
    lastNameValidator = (name) => {
        if (name.length < 3) {
            this.setState({
                error: {
                    ...this.state.error,
                    lastname: true,
                }
            })
        }
        else {
            this.setState({
                input: {
                    ...this.state.input,
                    lastname: name,
                },
                error: {
                    ...this.state.error,
                    lastname: false,
                }
            })
        }
    }
    emailValidator = (emailInput) => {
        this.setState({
            input: {
                ...this.state.input,
                email: emailInput,
            },
            error: {
                    ...this.state.error,
                    email: !validator.isEmail(emailInput),
            }
        })
    }
    passwordValidator = (passwordInput) => {
        this.setState({
            input: {
                    ...this.state.input,
                    password: passwordInput,
            },
            error: {
                    ...this.state.error,
                    password: !validator.isStrongPassword(passwordInput),
            }
        })
    }
    passwordConfirmation = (retypePasswordInput) => {
        this.setState({
            error: {
                    ...this.state.error,
                    confirmPass: !validator.equals(retypePasswordInput, this.state.input.password),
            }
        })
    }

    handleSubmit = (e) => {
    let errors = this.state.error;
    if(errors.firstname || errors.lastname || errors.email || errors.password || errors.confirmPass){
        e.preventDefault();
    }
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <InputComponent label="First Name :" type="text" handleValdiation={this.firstNameValidator} />
                    {this.state.error.firstname ? <div style={{ color: "red" }}> First Name Should be greater than 3 characters</div> : okDiv}

                    <InputComponent label="Last Name" type="text" handleValdiation={this.lastNameValidator} />
                    {this.state.error.lastname ? <div style={{ color: "red" }}> First Name Should be greater than 3 characters</div> : okDiv}

                    <InputComponent label="Your Email ID:" type="text" handleValdiation={this.emailValidator} />
                    {this.state.error.email ? <div style={{ color: "red" }}>Invalid Email</div> : okDiv}

                    <InputComponent label="Password" type="password" handleValdiation={this.passwordValidator} />
                    {this.state.error.password ?
                        <div style={{ color: "red" }}>Password Should Contain:
                            <ul>
                                <li>Mininmum Length of 8</li>
                                <li>Atleast 1 Lowercase character</li>
                                <li>Atleast 1 Upperrcase character</li>
                                <li>Atleast 1 Number </li>
                                <li>Atleast 1 symbol</li>
                            </ul></div> : okDiv}

                    <InputComponent label="Retype Password" type="password" handleValdiation={this.passwordConfirmation}/>
                    {this.state.error.confirmPass? <div style={{ color: "red" }}>Password Did Not Match</div> : okDiv}


                    <input type ="submit" className="submitBtn" value="Sign Up"></input>
                </form>
            </div>
        )
    }
}

const okDiv = <div style={{ color: "green" }}>OK!</div>;
export default SignUpForm
